//
//  GraphView.h
//  Calculator
//
//  Created by Daniel Hirschlein on 12/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GraphView;

@protocol GraphViewDataSource

- (NSArray *) plotsForGraphView:(GraphView *)sender;

@end

@interface GraphView : UIView

- (void) pinch:(UIPinchGestureRecognizer *)gesture;

@property (nonatomic, weak) IBOutlet id <GraphViewDataSource> dataSource;
@property (nonatomic) CGFloat scale;
@property (nonatomic) CGPoint origin;
@property (nonatomic) BOOL dotModeSelected;

@end
