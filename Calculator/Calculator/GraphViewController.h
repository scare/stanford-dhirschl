//
//  GraphViewController.h
//  Calculator
//
//  Created by Daniel Hirschlein on 12/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface GraphViewController : UIViewController <UISplitViewControllerDelegate>

@property (nonatomic, strong) id program;
@property (weak, nonatomic) IBOutlet UILabel *expressionLabel;
@property (nonatomic) BOOL dotModeSelected;

@end
