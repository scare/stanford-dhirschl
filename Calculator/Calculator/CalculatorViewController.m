//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Daniel Hirschlein on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GraphViewController.h"
#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController()
@property (nonatomic) BOOL userIsInTheMiddleOfTypingANumber;
@property (nonatomic) BOOL dotModeSelected;
@property (nonatomic, strong) CalculatorBrain *brain;
@property (nonatomic, strong) NSDictionary *testVariableValues;
- (void)executeTest;
@end

@implementation CalculatorViewController

@synthesize brain = _brain;
@synthesize description = _description;
@synthesize display = _display;
@synthesize testVariables = _testVariables;
@synthesize userIsInTheMiddleOfTypingANumber = _userIsInTheMiddleOfTypingANumber;
@synthesize testVariableValues = _testVariableValues;
@synthesize dotModeSelected = _dotModeSelected;

- (GraphViewController *)splitViewGraphViewController
{
    id gvc = [self.splitViewController.viewControllers lastObject];
    
    if(![gvc isKindOfClass:[GraphViewController class]])
        gvc = nil;
    
    return gvc;
}

- (CalculatorBrain *) brain
{
    if(_brain == nil)
        _brain = [[CalculatorBrain alloc]init];
    
    return _brain;       
}
- (IBAction)modeChanged:(UISwitch *)sender {
    self.dotModeSelected = sender.on;
}

- (IBAction)digitPressed:(UIButton *)sender {
    
    NSString *digit = sender.currentTitle; //[sender currentTitle];
    NSLog(@"digit pressed = %@", digit);
    
    if ([@"." isEqualToString:digit])
    {
        NSRange range = [self.display.text rangeOfString:@"."];
        
        if(range.location == NSNotFound)
        {
            if (self.userIsInTheMiddleOfTypingANumber)
                self.display.text = [self.display.text stringByAppendingString:digit];
            else
                self.display.text = @"0.";
                        
            self.userIsInTheMiddleOfTypingANumber = YES;
        }
    }
    else if(self.userIsInTheMiddleOfTypingANumber)
    {
        self.display.text = [self.display.text stringByAppendingString:digit];
    }
    else
    {
        self.display.text = digit;
        self.userIsInTheMiddleOfTypingANumber = YES;
    }
}

- (IBAction)undo {
    
    if(self.userIsInTheMiddleOfTypingANumber)
    {
        self.display.text = [self.display.text substringToIndex:[self.display.text length]-1];
        
        if([self.display.text length] == 0) 
        {
            [self.brain popOperand];
            self.display.text = [NSString stringWithFormat:@"%g", [CalculatorBrain runProgram:self.brain.program]];   
            self.userIsInTheMiddleOfTypingANumber = NO;
        }
    }
    else
    {
        [self.brain popOperand];
        self.display.text = [NSString stringWithFormat:@"%g", [CalculatorBrain runProgram:self.brain.program]]; 
        self.description.text = [CalculatorBrain descriptionOfProgram:self.brain.program];
    }
}


- (IBAction)enterPressed {
    
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInTheMiddleOfTypingANumber = NO;
    
    self.description.text = [CalculatorBrain descriptionOfProgram:self.brain.program];
}

- (IBAction)operationPressed:(UIButton *) sender {
    
    NSString *resultString;
    
    if(self.userIsInTheMiddleOfTypingANumber) [self enterPressed];
    
    id result = [self.brain performOperation:sender.currentTitle];
    
    if([result isKindOfClass:[NSString class]])
        resultString = result;
    else if([result isKindOfClass:[NSNumber class]])
        resultString = [NSString stringWithFormat:@"%g", [result doubleValue]];

    self.display.text = resultString;    
    
    self.description.text = [CalculatorBrain descriptionOfProgram:self.brain.program];
}

- (IBAction)clearContents {

    [self.brain clearMemory];
    self.userIsInTheMiddleOfTypingANumber = NO;
    self.description.text = [CalculatorBrain descriptionOfProgram:self.brain.program];
    self.display.text = @"0";
    self.testVariables.text = @"";
}

- (IBAction)changeSign {

    if(self.userIsInTheMiddleOfTypingANumber)
    {
        double displayValue = [self.display.text doubleValue];
        
        displayValue *= -1;
        
        self.display.text = [NSString stringWithFormat:@"%g", displayValue];
    }
}

- (IBAction)variablePressed:(UIButton *)sender {
    if(self.userIsInTheMiddleOfTypingANumber)
        [self enterPressed];
    
    
    self.userIsInTheMiddleOfTypingANumber = NO;
    [self.brain pushVariable:sender.currentTitle];
    
    self.display.text = sender.currentTitle;
    self.description.text = [CalculatorBrain descriptionOfProgram:self.brain.program];
}

- (IBAction)graphPressed {
        
    GraphViewController *gvc = [self splitViewGraphViewController]; 
    
    if(gvc)
    {
        [gvc setDotModeSelected:self.dotModeSelected];
        [gvc setProgram:self.brain.program];
        
        gvc.expressionLabel.text =  [CalculatorBrain descriptionOfProgram:self.brain.program]; 
    }
    else
    {
        [self performSegueWithIdentifier:nil sender:self];
    }
}

- (IBAction)test1 {
    
    self.testVariableValues = [NSDictionary dictionaryWithObjectsAndKeys:nil, @"x", nil, @"y", nil, @"z", nil];
    [self executeTest];
}

- (IBAction)test2 {
    self.testVariableValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:3], @"x", [NSNumber numberWithDouble:4], @"y", [NSNumber numberWithDouble:-1.1], @"z",nil];
    [self executeTest];

}

- (IBAction)test3 {
    self.testVariableValues = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:-4], @"x", [NSNumber numberWithDouble:3], @"y", [NSNumber numberWithDouble:-21.3], @"z",  nil];
    [self executeTest];

}

- (void)executeTest
{
    self.testVariables.text = @"";
    for(NSString *var in [CalculatorBrain variablesUsedInProgram:self.brain.program])
    {
        self.testVariables.text = [self.testVariables.text stringByAppendingString: [NSString stringWithFormat:@"%@: %g ", var, [[self.testVariableValues objectForKey:var] doubleValue]]];
    }
    
    self.display.text = [NSString stringWithFormat:@"%g", [CalculatorBrain runProgram:self.brain.program usingVariableValues:self.testVariableValues]];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    GraphViewController *vc = segue.destinationViewController;
    
    vc.title = [CalculatorBrain descriptionOfProgram:self.brain.program];
    vc.expressionLabel.text =  [CalculatorBrain descriptionOfProgram:self.brain.program];
    
    [vc setDotModeSelected:self.dotModeSelected];
    [vc setProgram:self.brain.program];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
         return YES;
     else 
         return NO;
}

- (void)viewDidUnload {
    [self setTestVariables:nil];
    [super viewDidUnload];
}
@end
