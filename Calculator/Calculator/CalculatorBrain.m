//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Daniel Hirschlein on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CalculatorBrain.h"
#import "Math.h"

@interface CalculatorBrain()

+ (NSString*) descriptionOfTopOfStack:(NSMutableArray *) stack;
+ (BOOL) isOperation:(NSString *)operation;
+ (BOOL) isEnclosedInParenthesis:(NSString *)expression;

@property (nonatomic, strong) NSMutableArray *programStack;

@end

@implementation CalculatorBrain

@synthesize programStack = _programStack;

- (NSMutableArray *)programStack
{
    if(_programStack == nil) _programStack = [[NSMutableArray alloc] init];
    
    return _programStack;
}

- (void)pushOperand:(double)operand
{
    [self.programStack addObject:[NSNumber numberWithDouble:operand]];
}

- (void)pushVariable:(NSString *)var
{
    [self.programStack addObject:var];
}

- (void)popOperand
{
    [self.programStack removeLastObject];
}

- (id)performOperation:(NSString *)operation
{
    [self.programStack addObject:operation];
    return [CalculatorBrain runProgram:self.program];
}

- (id)program
{
    return [self.programStack copy];
}



+ (BOOL) isEnclosedInParenthesis:(NSString *)expression
{
    BOOL result = NO;
    
    if([expression hasPrefix:@"("])
    {
            if([expression hasSuffix:@")"])
                return YES;
    }
    return result;
        
}

+ (BOOL) isOperation:(NSString *)operation
{
    BOOL result = NO;
    
    if([operation isEqualToString:@"+"]){
        result = YES;
    }
    else if([operation isEqualToString:@"*"]){
            result = YES;
    }
    else if([operation isEqualToString:@"-"]){
            result = YES;
    }
    else if([operation isEqualToString:@"/"]){
        result = YES;
      
    }
    else if([operation isEqualToString:@"sin"]){
        result = YES;
    }
    else if([operation isEqualToString:@"cos"]){
                result = YES;
    }
    else if([operation isEqualToString:@"sqrt"]){
        result = YES;
      }
    else if([operation isEqualToString:@"π"]){
        result = YES;
    }   
    return result;
}


+ (NSSet *)variablesUsedInProgram:(id)program;
{
    NSMutableSet* variables = nil;
    
    for(id item in program)
    {
        if([item isKindOfClass:[NSString class]])
        {
            if(![self isOperation:item])
            {
                if(variables == nil)
                    variables = [[NSMutableSet alloc]init];
                [variables addObject:item];
            }
        }
    }
    
    return [NSSet setWithSet:variables];
}

+ (NSString*) descriptionOfTopOfStack:(NSMutableArray *) stack
{
    NSString* result;
    
    id topOfStack = [stack lastObject];
    if (topOfStack) [stack removeLastObject];
    
    if([topOfStack isKindOfClass:[NSNumber class]])
    {
        result = [topOfStack stringValue];
    }
    else if([topOfStack isKindOfClass:[NSString class]])
    {
        NSString *operation = topOfStack;
        
        //calculate result 
        if([operation isEqualToString:@"+"]){
            
            NSString* addend1 = [self descriptionOfTopOfStack:stack];
            NSString* addend2 = [self descriptionOfTopOfStack:stack];
            
            if(addend1 == nil || addend2 == nil)
            {
                result = @"Error";
                [stack removeAllObjects];
            }
            else
            {
                result = [NSString stringWithFormat:@"(%@ + %@)",     
                addend2, addend1];       
            }
        }
        else if([operation isEqualToString:@"*"]){
            
            NSString* multiplier1 = [self descriptionOfTopOfStack:stack];
            NSString* multiplier2 = [self descriptionOfTopOfStack:stack];
            
            if(multiplier1 == nil || multiplier2 == nil)
            {
                result = @"Error";
                [stack removeAllObjects];
            }
            else{
                result = [NSString stringWithFormat:@"%@ * %@",     
                          multiplier2, multiplier1];
            }
        }
        else if([operation isEqualToString:@"-"]){
            
            
            NSString* subtrahend1 = [self descriptionOfTopOfStack:stack];
            NSString* subtrahend2 = [self descriptionOfTopOfStack:stack];
            
            if(subtrahend1 == nil || subtrahend2 == nil)
            {
                result = @"Error";
                [stack removeAllObjects];
            }
            else{
                    result = [NSString stringWithFormat:@"(%@ - %@)",     
                          subtrahend2, subtrahend1];            
            }
            
        }
        else if([operation isEqualToString:@"/"]){
            
            id denominator = [self descriptionOfTopOfStack:stack];
            id numerator = [self descriptionOfTopOfStack:stack];
            
            if(numerator == nil || denominator == nil)
            {
                result = @"Error";
                [stack removeAllObjects];
            }
            //else if([denominator doubleValue] == 0){
            //    result = @"Error";
            //    [stack removeAllObjects];
            //}
            else{
                result = [NSString stringWithFormat:@"%@ / %@",     
                          numerator,denominator];
            }
        }
        else if([operation isEqualToString:@"sin"]){
            
            NSString *value = [self descriptionOfTopOfStack:stack];
            
            if (value == nil)
            {
                result = @"Error";
                [stack removeAllObjects];
            }
            else if([self isEnclosedInParenthesis:value])
                result = [NSString stringWithFormat:@"sin%@", value];
            else
                result = [NSString stringWithFormat:@"sin(%@)", value];
        }
        else if([operation isEqualToString:@"cos"]){
            NSString *value = [self descriptionOfTopOfStack:stack];
            
            if (value == nil)
            {
                result = @"Error";
                [stack removeAllObjects];
            }
            else if([self isEnclosedInParenthesis:value])
                result = [NSString stringWithFormat:@"cos%@", value];
            else
                result = [NSString stringWithFormat:@"cos(%@)", value];      
        }
        else if([operation isEqualToString:@"sqrt"]){
            NSString *value = [self descriptionOfTopOfStack:stack];
            
            if (value == nil)
            {
                result = @"Error";
                [stack removeAllObjects];
            }
            else if([self isEnclosedInParenthesis:value])
                result = [NSString stringWithFormat:@"sqrt%@", value];
            else
                result = [NSString stringWithFormat:@"sqrt(%@)", value];      
        }
        else if([operation isEqualToString:@"π"]){
            result = @"π";
        } 
        else
            result = operation;
    }
        
    return result;
}


+ (NSString *)descriptionOfProgram:(id)program
{
    NSMutableArray *stack;
    
    if([program isKindOfClass:[NSArray class]]){
        stack = [program mutableCopy];
    }
    
    NSString *result = [self descriptionOfTopOfStack:stack]; 
    
    NSLog(@"%@", result);
    
    while([stack lastObject])
    {
        result = [NSString stringWithFormat:@"%@, %@", result, [self descriptionOfTopOfStack:stack]];
        NSLog(@"%@", result);
    }
    return result;
}


+ (id) popOperandOffStack:(NSMutableArray *) stack
{
    id result = 0;
    
    id topOfStack = [stack lastObject];
    if (topOfStack) [stack removeLastObject];
    
    if([topOfStack isKindOfClass:[NSNumber class]])
    {
        result = [NSNumber numberWithDouble:[topOfStack doubleValue]];
    }
    else if([topOfStack isKindOfClass:[NSString class]])
    {
        NSString *operation = topOfStack;
        
        //calculate result 
        if([operation isEqualToString:@"+"]){
            
            id addend1 = [self popOperandOffStack:stack];
            id addend2 = [self popOperandOffStack:stack];
            
            if(addend1 == nil || addend2 == nil)
            {
                result = @"Error: Insufficient operands";
                [stack removeAllObjects];
            }
            else{
                double add = [addend1 doubleValue] + [addend2 doubleValue];
                result = [NSNumber numberWithDouble:add];
            }
        }
        else if([operation isEqualToString:@"*"]){
           
            
            id multiplier1 = [self popOperandOffStack:stack];
            id multiplier2 = [self popOperandOffStack:stack];
            
            if(multiplier1 == nil || multiplier2 == nil)
            {
                result = @"Error: Insufficient operands";
                [stack removeAllObjects];
            }
            else{
                double multiply = [multiplier1 doubleValue] * [multiplier2 doubleValue];
                result = [NSNumber numberWithDouble:multiply];
            }
        }
        else if([operation isEqualToString:@"-"]){
            
            id subtrahend1 = [self popOperandOffStack:stack];
            id subtrahend2 = [self popOperandOffStack:stack];
            
            if(subtrahend1 == nil || subtrahend2 == nil)
            {
                result = @"Error: Insufficient operands";
                [stack removeAllObjects];
            }
            else{
                double subtract = [subtrahend2 doubleValue] - [subtrahend1 doubleValue];
                result = [NSNumber numberWithDouble:subtract];
            }
        }
        else if([operation isEqualToString:@"/"]){
            
            id denominator = [self popOperandOffStack:stack];
            id numerator = [self popOperandOffStack:stack];
            
            if(numerator == nil || denominator == nil)
            {
                result = @"Error: Insufficient operands";
                [stack removeAllObjects];
            }
            else if([denominator doubleValue] == 0){
                result = @"Error: Division by Zero";
                [stack removeAllObjects];
            }
            else{
                double division = [numerator doubleValue] / [denominator doubleValue];
                result = [NSNumber numberWithDouble:division];
            }
        }
        else if([operation isEqualToString:@"sin"]){
            
            id arg = [self popOperandOffStack:stack];
            
            if(arg == nil)
            {
                result = @"Error: Insufficient operands";
                [stack removeAllObjects];
            }
            else
                result = [NSNumber numberWithDouble:sin([arg doubleValue])];
        }
        else if([operation isEqualToString:@"cos"]){
            id arg = [self popOperandOffStack:stack];
            
            if(arg == nil)
            {
                result = @"Error: Insufficient operands";
                [stack removeAllObjects];
            }
            else
                result = [NSNumber numberWithDouble:cos([arg doubleValue])];
        }
        else if([operation isEqualToString:@"sqrt"]){
            id arg = [self popOperandOffStack:stack];
            
            if(arg == nil)
            {
                result = @"Error: Insufficient operands";
                [stack removeAllObjects];
            }
            else if([arg doubleValue] < 0)
            {
                result = @"Error: Sqrt of Negative Number";
                [stack removeAllObjects];
            }
            else
                result = [NSNumber numberWithDouble:sqrt([arg doubleValue])];
        }
        else if([operation isEqualToString:@"π"]){
            result = [NSNumber numberWithDouble:M_PI];
        }   
    }
    return result;
}

+ (id)runProgram:(id)program
{
    return [CalculatorBrain runProgram:program usingVariableValues:nil];
}

+ (id)runProgram:(id)program usingVariableValues:(NSDictionary *)variableValues
{
    NSMutableArray *stack;
    
    if([program isKindOfClass:[NSArray class]]){
        stack = [program mutableCopy];
        
        NSSet* variables = [self variablesUsedInProgram:program];
        
        for(int i = 0; i < [stack count]; i++)
        {
            id item = [stack objectAtIndex:i];
            
            if([item isKindOfClass:[NSString class]])
            {
                if(![self isOperation:item])
                {
                    NSNumber* variableValue;
                    
                    if([variables member:item]){
                        
                        variableValue = [variableValues objectForKey:item];
                        if(variableValue == nil)
                            variableValue = [NSNumber numberWithDouble:0];
                        
                        [stack replaceObjectAtIndex:i withObject:variableValue];
                    }
                }
            }
        }
    }
    return [self popOperandOffStack:stack];
}


- (void)clearMemory
{
    [self.programStack removeAllObjects];
}


@end
