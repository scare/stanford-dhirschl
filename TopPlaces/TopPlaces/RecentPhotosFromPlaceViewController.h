//
//  RecentPhotosFromPlaceViewController.h
//  TopPlaces
//
//  Created by Daniel Hirschlein on 1/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentPhotosFromPlaceViewController : UITableViewController

@property (nonatomic, strong) NSDictionary* place;

@end
