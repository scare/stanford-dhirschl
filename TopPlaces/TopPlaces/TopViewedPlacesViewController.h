//
//  TopViewedPlacesViewController.h
//  TopPlaces
//
//  Created by Daniel Hirschlein on 1/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopViewedPlacesViewController : UITableViewController

@property (nonatomic, strong) NSArray *places; // of Flickr photo dictionaries

@end
