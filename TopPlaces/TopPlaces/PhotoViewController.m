//
//  PhotoViewController.m
//  TopPlaces
//
//  Created by Daniel Hirschlein on 1/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoViewController.h"
#import "FlickrFetcher.h"
#import "TopPlacesConstants.h"

@interface PhotoViewController() <UIScrollViewDelegate, UISplitViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation PhotoViewController
@synthesize scrollView = _scrollView;
@synthesize photo = _photo;
@synthesize imageView = _imageView;

- (void)addPhotoToRecentlyViewedList
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *recentlyViewedPhotos = [[defaults objectForKey:RECENT_PHOTOS_KEY] mutableCopy];
    
    if(!recentlyViewedPhotos) recentlyViewedPhotos = [NSMutableArray arrayWithCapacity:20];
    
    if(![recentlyViewedPhotos containsObject:self.photo])
    {
        if(recentlyViewedPhotos.count == 20)
            [recentlyViewedPhotos removeObjectAtIndex:0];           
        
        [recentlyViewedPhotos addObject:self.photo];
        [defaults setObject:recentlyViewedPhotos forKey:RECENT_PHOTOS_KEY];
        [defaults synchronize];
    }
}


- (void)setPhoto:(NSDictionary *)photo
{
    _photo = photo;
    
    [self addPhotoToRecentlyViewedList];
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("flickr downloader", NULL);
    dispatch_async(downloadQueue, ^{
        
        NSURL *url = [FlickrFetcher urlForPhoto:photo format:FlickrPhotoFormatLarge];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(),^{
           
            self.scrollView.zoomScale = 1;
            
            self.imageView.image =  [UIImage imageWithData:data];
            self.imageView.frame = CGRectMake(0, 0, self.imageView.image.size.width, self.imageView.image.size.height);
       
            self.scrollView.delegate = self;    
            self.scrollView.contentSize = self.imageView.image.size;
        });
    });
    dispatch_release(downloadQueue);
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    self.splitViewController.delegate = self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - UISplitViewControllerDelegate

-(BOOL) splitViewController:(UISplitViewController *)svc 
shouldHideViewController:(UIViewController *)vc 
inOrientation:(UIInterfaceOrientation)orientation
{
    if (UIInterfaceOrientationIsPortrait(orientation))
        return YES;
    else return NO;
}

- (void)splitViewController:(UISplitViewController *)svc 
     willHideViewController:(UIViewController *)aViewController 
          withBarButtonItem:(UIBarButtonItem *)barButtonItem 
       forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = @"Top Places";
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)splitViewController:(UISplitViewController *)svc 
     willShowViewController:(UIViewController *)aViewController 
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // tell the detail viwe to put this button away
    self.navigationItem.leftBarButtonItem = nil;
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


- (void)viewDidLoad
{
    [super viewDidLoad];
  
  
}


- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}





@end
