//
//  Photographer.m
//  PhotoMania
//
//  Created by Daniel Hirschlein on 1/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Photographer.h"
#import "Photo.h"


@implementation Photographer

@dynamic name;
@dynamic photos;

@end
