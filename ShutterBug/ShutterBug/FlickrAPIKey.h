//
//  FlickrAPIKey.h
//
//  Created for Stanford CS193p Fall 2011.
//  Copyright 2011 Stanford University. All rights reserved.
//
//  Get your own key!
//  No Flickr fetches will work without the API Key!
//


// Get your own API Key at: http://www.flickr.com/services/apps/create/apply/
#define FlickrAPIKey @""
